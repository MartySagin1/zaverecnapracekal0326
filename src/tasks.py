def is_palindrome(data):
    """
    Returns True if `data` is a palindrome and False otherwise.
    Hint:
        slicing is your friend, use it
    Example:
        is_palindrome('aba') == True
        is_palindrome('abc') == False
    """

    return data == data[::-1]


def lex_compare(a, b):
    """
    Lexicographically compare `a` with `b` and return the smaller string.
    Implement the comparison yourself, do not use the `<` operator for comparing strings :)

    Example:
        lex_compare('a', 'b') == 'a'
        lex_compare('ahoj', 'buvol') == 'ahoj'
        lex_compare('ahoj', 'ahojky') == 'ahoj'
        lex_compare('dum', 'automobil') == 'automobil'
    """
    if a == '' or b == '':
        return ''

    if len(a) < len(b):
        temp = a
        a = b
        b = temp

    for index in range(0, len(a)):
        if a[index] > b[index]:
            return b
        elif a[index] < b[index]:
            return a
        
        if (len(b) - 1) < (index + 1):
            return b


def count_successive(string):
    """
    Go through the string and for each character, count how many times it appears in succession.
    Store the character and the count in a tuple and return a list of such tuples.

    Example:
          count_successive("aaabbcccc") == [("a", 3), ("b", 2), ("c", 4)]
          count_successive("aba") == [("a", 1), ("b", 1), ("a", 1)]
    """
    result = []
    
    if string == "":
        return result
    
    resultChar = string[0]
    resultCount = 0

    for oneChar in string:
        if oneChar == resultChar:
            resultCount += 1
        else:
            result.append((resultChar, resultCount))
            resultChar = oneChar
            resultCount = 1

    result.append((resultChar, resultCount))

    return result

def find_positions(items):
    """
    Go through the input list of items and collect indices of each individual item.
    Return a dictionary where the key will be an item and its value will be a list of indices
    where the item was found.

    Example:
        find_positions(["hello", 1, 1, 2, "hello", 2]) == {
            2: [3, 5],
            "hello": [0, 4],
            1: [1, 2]
        }
    """
    result = dict()

    for index, item in enumerate(items):
        if item not in result:
            result.update({item: [index]})
        else:
            result[item].append(index)

    return result

def invert_dictionary(dictionary):
    """
    Invert the input dictionary. Turn keys into values and vice-versa.
    If more values would belong to the same key, return None.

    Example:
        invert_dictionary({1: 2, 3: 4}) == {2: 1, 4: 3}
        invert_dictionary({1: 2, 3: 2}) is None
    """

    items = dictionary.items()

    result = dict()

    for tuple in items:
        if tuple[1] not in result:
            result.update({tuple[1]: tuple[0]})
        else:
            return None
        
    return result

