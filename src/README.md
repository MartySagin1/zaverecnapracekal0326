# Projekt SKJ KAL0326 - Space Game (PyGame)

## Jak hru spustit?

1. Stáhneme soubory s hrou a vložíme je například na plochu.
2. Otevřeme Visual Studio Code a otevřeme složku se soubory hry.
3. Nainstalujeme Python, pokud ho nemáme: [Python.org](https://www.python.org)
4. Pokud nemáme nainstalovaný PyGame, nainstalujeme PyGame:
   - Otevřeme terminál ve VS Code a napíšeme: `pip install pygame`
   - Případně si můžeme projít [tutorial na PyPI](https://pypi.org/project/pygame)
5. Nastavíme virtuální prostředí venv:
   - Otevřeme terminál ve VS Code a napíšeme: `python -m venv .venv`
6. Ve VS Code otevřeme soubor `space_game.py`.
7. Poté klikneme na tlačítko `Run` ve VS Code a hra se spustí.

## Informace o hře

Cíl hry je nasbírat co největší skóre. Pokud skóre, které jsme nasbírali během hry, bude větší než "Highscore", tak se po "úmrtí" hráče nastaví toto skóre jako nové Highscore.

### Jak hru hrát?

- Pohyb hráče je pomocí šipek:
  - Šipka nahoru: rozpohybuje hráče
  - Šipka doleva / doprava: otáčení hráče
- Pomocí klávesy `Spacebar` střílíme.
- Pomocí klávesy `S` ukládáme aktuální stav hry.
- Pomocí klávesy `ESC` se ze hry přesuneme do hlavního menu.
- Pomocí klávesy `R` při konci hry restartujeme hru.

Hráč má maximálně 3 životy.

### Typy nepřátel

#### Asteroid

- Když asteroid zničíme, dostaneme 10 skóre.
- Pokud se asteroid dotkne hráče, tak hráč na určitou dobu zamrzne (doba záleží na velikosti asteroidu -> čím větší asteroid, tím delší doba zamrznutí).
- Když zničíme asteroid, je zde šance na spawn power-upu (power-upy vysvětleny níže).

#### UFO

- Když UFO zničíme, dostaneme 100 skóre.
- Tento nepřítel lítá náhodně po mapě a za určitý čas střílí po hráči.
- Pokud se hráč tohoto nepřítele dotkne, tak na určitou dobu zamrzne.
- Pokud se hráč dotkne střely nepřítele, tak se mu ubere jeden život.
- Po zničení UFA je nějaká šance na spawn srdíčka, které může hráč vzít a doplnit si tak životy.

#### Ninja (Shuriken)

- Když Ninju zničíme, dostaneme 150 skóre.
- Tento nepřítel lítá za hráčem.
- Postupem své existence je rychlejší a rychlejší.
- Pokud se dotkne hráče, tak hráč na 1 sekundu zamrzne a ztrácí život.
- V momentě dotyku se Ninja sám zničí -> hráč tímto způsobem nedostane 150 skóre.
- Po zničení Ninjy je nějaká šance na spawn srdíčka, které může hráč vzít a doplnit si tak životy.

### Stavy hráče

#### Nesmrtelnost

- Tento stav nastává, když se hráč dotkne UFA, Ninjy nebo nepřátelské střely.
- Trvá určitou dobu -> v této době hráče nemůže nic zranit a nic se ho nemůže dotknout.

#### Zamrznutí

- V tomto stavu se hráč nemůže hýbat ani střílet.
- Může však přijít o životy, pokud se ho dotkne nepřítel nebo nepřátelská střela.

### Power-upy

Power-upy se spawnují náhodně po zničení asteroidů. Power-up bude po spawnutí na obrazovce 10 sekund. Poté zmizí.

#### Štít

- Po sebrání štítu je hráč na 10 sekund ve stavu nesmrtelnosti.

#### Rychlejší střílení

- Po sebrání tohoto power-upu bude moct hráč rychleji střílet na 10 sekund.

#### Double Damage

- Po sebrání tohoto power-upu bude mít hráč 2x větší poškození na 10 sekund.

#### Full Heal

- Po sebrání tohoto power-upu bude hráč plně uzdraven na 3 životy.
