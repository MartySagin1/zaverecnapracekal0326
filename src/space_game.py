import pygame
import sys
import math
import time
import random
import pickle

# Initialize Pygame
pygame.init()

# Constants for display
WIDTH, HEIGHT = 1920, 1080
FPS = 60


# Constants for player ship
SHIP_WIDTH, SHIP_HEIGHT = 48, 48  
FRAME_COUNT = 4  
SHIP_SPEED = 3
ROTATION_SPEED = 5  
BULLET_SPEED = 15 
BULLET_WIDTH, BULLET_HEIGHT = 8, 8  
WRAP_BUFFER_Y = -80  
WRAP_BUFFER_X = -160



# Rotation for fixed ship image
FIXED_ROTATION = 270



# Constants for asteroid animation
ASTEROID_ROWS = 4
ASTEROID_COLS = 8
ASTEROID_FRAME_WIDTH = 62  # Šířka jednoho snímku asteroidu
ASTEROID_FRAME_HEIGHT = 62  # Výška jednoho snímku asteroidu



# Constants for asteroid
ASTEROID_SPAWN_DELAY = 7  # Čas v sekundách pro pokus o spawn asteroidu
ASTEROID_SPAWN_CHANCE = 0.3  # 30% šance na spawn
ASTEROID_SPEED = 3  # Rychlost pohybu asteroidu
MIN_ASTEROID_HEALTH = 1
MAX_ASTEROID_HEALTH = 3



# Constants for enemy
ENEMY_WIDTH, ENEMY_HEIGHT = 48, 48
ENEMY_FRAME_COUNT = 4  # Počet snímků v animaci nepřítele
ENEMY_SPAWN_DELAY = 30  # Každých 30 sekund
ENEMY_HEALTH_MIN = 2
ENEMY_HEALTH_MAX = 5
ENEMY_SPEED = 2  # Rychlost pohybu nepřítele



# Constants for enemy bullets
ENEMY_BULLET_SPEED = 3
ENEMY_BULLET_WIDTH, ENEMY_BULLET_HEIGHT = 8, 8
ENEMY_BULLET_SPAWN_MIN, ENEMY_BULLET_SPAWN_MAX = 4, 8



# Constatns for hearts
HEART_DURATION = 10  
HEART_BLINK_START = 3  
HEART_OPACITY = 192  
HEART_SPAWN_CHANCE = 0.1 
MAX_LIVES = 3 


# Constants for add score
ASTEROID_SCORE = 10 
ENEMY_SCORE = 100 
ENEMY_FOLLOW_SCORE = 150

# Power-up constants
POWER_UP_DURATION = 10
POWER_UP_BLINK_START = 3  
POWER_UP_SPAWN_CHANCE = 0.1  


# Active power-up state
active_power_up = None
power_up_end_time = 0


# Score
score = 0

# Highscore
highscore = 0
highscore_file = "highscore.pkl"


# Score font
score_font = pygame.font.Font(None, 36)


# State game variables
STATE_MENU = "menu"
STATE_PLAYING = "playing"
STATE_QUIT = "quit"
game_state = STATE_MENU


# Frozen state variables
player_frozen = False
freeze_end_time = 0
frozen_opacity = 192  # 75% opacity
blinking_toggle = False


# Count of player lives
player_lives = 3
game_over = False


# Length of invincibility
invincibility_duration = 3
invincible_end_time = 0



# Set up the display
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("SKJ Project - KAL0326")
clock = pygame.time.Clock()

# Load background image 
background = pygame.image.load("images/space_background.jpg")
background = pygame.transform.scale(background, (WIDTH, HEIGHT))

# Load player ship image
ship_sheet = pygame.image.load("images/player_ship.png").convert_alpha()
ship_sheet = pygame.transform.scale(ship_sheet, (SHIP_WIDTH * FRAME_COUNT, SHIP_HEIGHT))

ship_frames = [
    pygame.transform.rotate(
        ship_sheet.subsurface(pygame.Rect(SHIP_WIDTH * i, 0, SHIP_WIDTH, SHIP_HEIGHT)), FIXED_ROTATION
    )
    for i in range(FRAME_COUNT)
]

# Load bullet image
bullet_image = pygame.image.load("images/player_bullet_reform.png").convert_alpha()
bullet_image = pygame.transform.scale(bullet_image, (BULLET_WIDTH, BULLET_HEIGHT))


# List of active asteroids
asteroids = []

# Timer for asteroid spawning
last_spawn_time = 0

# Load asteroid image
asteroid_sheet = pygame.image.load("images/rock_image.png").convert_alpha()

asteroid_frames = [
    asteroid_sheet.subsurface(pygame.Rect(
        ASTEROID_FRAME_WIDTH * col,
        ASTEROID_FRAME_HEIGHT * row,
        ASTEROID_FRAME_WIDTH,
        ASTEROID_FRAME_HEIGHT
    ))
    for row in range(ASTEROID_ROWS)
    for col in range(ASTEROID_COLS)
]


# List of active hearts
hearts = []

# Load heart image
heart_image = pygame.image.load('images/heart_image.png').convert_alpha()
heart_image = pygame.transform.scale(heart_image, (24, 16))


# List of active power-ups
power_ups = []

# Load power-up images
shooting_pwr_up_img = pygame.image.load('images/shooting_pwr_up.png').convert_alpha()
shooting_pwr_up_img = pygame.transform.scale(shooting_pwr_up_img, (48, 48))

shield_pwr_up_img = pygame.image.load('images/shield_pwr_up.png').convert_alpha()
shield_pwr_up_img = pygame.transform.scale(shield_pwr_up_img, (48, 48))

dmg_pwr_up_img = pygame.image.load('images/dmg_pwr_up4.png').convert_alpha()
dmg_pwr_up_img = pygame.transform.scale(dmg_pwr_up_img, (48, 48))

# Double damage power-up
double_damage = False
dmg_pwr_up_end_time = 0


heal_pwr_up_img = pygame.image.load('images/heal_pwr_up.png').convert_alpha()
heal_pwr_up_img = pygame.transform.scale(heal_pwr_up_img, (48, 48))




# Nastavení výchozí pozice a úhlu lodi
ship_x, ship_y = WIDTH // 2, HEIGHT // 2
ship_angle = 90  # Úhel natočení lodi (počáteční natočení o 90 stupňů)
frame_index = 0
frame_delay = 10  # Zpoždění mezi snímky animace
frame_counter = 0

# Časovač střelby
last_shot_time = 0
shot_delay = 1  # Zpoždění mezi střelami v sekundách

# Seznam aktivních střel
bullets = []

# Konstanty pro animaci asteroidu
ASTEROID_ANIM_DELAY = 5  # Zpoždění mezi snímky animace asteroidu
asteroid_anim_index = 0  # Počáteční index animace
asteroid_anim_counter = 0  # Počítadlo pro sledování zpoždění




# Enemy list and enemy bullet list
enemies = []
enemy_bullets = []

# Last enemy spawn time
last_enemy_spawn_time = 0

# Load enemy image
enemy_sheet = pygame.image.load("images/enemy_shooter.png").convert_alpha()
enemy_sheet = pygame.transform.scale(enemy_sheet, (ENEMY_WIDTH * ENEMY_FRAME_COUNT, ENEMY_HEIGHT))

# Load enemy frames
enemy_frames = [
    enemy_sheet.subsurface(pygame.Rect(ENEMY_WIDTH * i, 0, ENEMY_WIDTH, ENEMY_HEIGHT))
    for i in range(ENEMY_FRAME_COUNT)
]

# Load enemy bullet image
enemy_bullet_image = pygame.image.load("images/enemy_bullet.png").convert_alpha()
enemy_bullet_image = pygame.transform.scale(enemy_bullet_image, (ENEMY_BULLET_WIDTH, ENEMY_BULLET_HEIGHT))

# Constant for enemy animation
ENEMY_ANIM_DELAY = 5  
enemy_anim_index = 0  
enemy_anim_counter = 0  


# Constants for enemy follower
ENEMY_FOLLOW_WIDTH, ENEMY_FOLLOW_HEIGHT = 48, 48
ENEMY_FOLLOW_FRAME_COUNT = 4
ENEMY_FOLLOW_HEALTH = 2
ENEMY_FOLLOW_DEFAULT_SPEED = 0.5
ENEMY_FOLLOW_MAX_SPEED = SHIP_SPEED + 0.5
ENEMY_FOLLOW_SPEED_INCREMENT = 0.15


# List of enemy followers
enemy_follows = []

# Load the enemy follower sprite sheet
enemy_follow_sheet = pygame.image.load("images/enemy_follow.png").convert_alpha()

# Set the original width and height for each frame in the sprite sheet
original_enemy_follow_width = 32
original_enemy_follow_height = 32

# Rotate and scale each frame
enemy_follow_frames = [
    pygame.transform.scale(
        enemy_follow_sheet.subsurface(pygame.Rect(original_enemy_follow_width * i, 0, original_enemy_follow_width, original_enemy_follow_height)),
        (ENEMY_FOLLOW_WIDTH, ENEMY_FOLLOW_HEIGHT)
    )
    for i in range(4)
]


# Timer for enemy follower spawn
ENEMY_FOLLOW_SPAWN_DELAY = 50
last_enemy_follow_spawn_time = 0

# Probability for heart spawn on player kill
ENEMY_FOLLOW_HEART_SPAWN_CHANCE = 0.25

# Load explosion image and create frames
EXPLOSION_ROWS = 8
EXPLOSION_COLS = 8
EXPLOSION_FRAME_WIDTH = 512  
EXPLOSION_FRAME_HEIGHT = 512 
EXPLOSION_SCALED_WIDTH = 512 
EXPLOSION_SCALED_HEIGHT = 512  

# Constants for explosion animation
EXPLOSION_ANIM_DELAY = 1

# List to store active explosions
explosions = []


# Load the explosion sprite sheet
explosion_sheet = pygame.image.load("images/enemy_explosion.png").convert_alpha()

# Extract individual frames
explosion_frames = [
    pygame.transform.scale(
        explosion_sheet.subsurface(pygame.Rect(
            EXPLOSION_FRAME_WIDTH * col,
            EXPLOSION_FRAME_HEIGHT * row,
            EXPLOSION_FRAME_WIDTH,
            EXPLOSION_FRAME_HEIGHT
        )),
        (EXPLOSION_SCALED_WIDTH, EXPLOSION_SCALED_HEIGHT)
    )
    for row in range(EXPLOSION_ROWS)
    for col in range(EXPLOSION_COLS)
]


# Constants for asteroid explosion animation
ASTEROID_EXPLOSION_ROWS = 8
ASTEROID_EXPLOSION_COLS = 8
ASTEROID_EXPLOSION_FRAME_WIDTH = 512
ASTEROID_EXPLOSION_FRAME_HEIGHT = 512
ASTEROID_EXPLOSION_X_SCALE = 128
ASTEROID_EXPLOSION_Y_SCALE = 128

# Asteroid explosion image
asteroid_explosion_sheet = pygame.image.load("images/asteroid_explosion.png").convert_alpha()

asteroid_explosion_frames = [
    pygame.transform.scale(
        asteroid_explosion_sheet.subsurface(pygame.Rect(
            ASTEROID_EXPLOSION_FRAME_WIDTH * col,
            ASTEROID_EXPLOSION_FRAME_HEIGHT * row,
            ASTEROID_EXPLOSION_FRAME_WIDTH,
            ASTEROID_EXPLOSION_FRAME_HEIGHT
        )),
    (ASTEROID_EXPLOSION_X_SCALE, ASTEROID_EXPLOSION_Y_SCALE))
    for row in range(ASTEROID_EXPLOSION_ROWS)
    for col in range(ASTEROID_EXPLOSION_COLS)
]


# Timers for difficulty increase
difficulty_timer = 0
difficulty_interval = 45  


# Count of asteroid spawners
asteroid_spawners = 1
max_asteroid_spawners = 5

# Count of enemy spawners
enemy_spawners = 1
max_enemy_spawners = 3


# Chance for spawning hearts and power-ups
current_heart_spawn_chance = HEART_SPAWN_CHANCE
current_power_up_spawn_chance = POWER_UP_SPAWN_CHANCE

max_heart_spawn_chance = 0.5
max_power_up_spawn_chance = 0.5



# Health and speed increments for enemies
current_enemy_health_max = ENEMY_HEALTH_MAX
current_enemy_speed = ENEMY_SPEED

enemy_health_increment = 1
enemy_speed_increment = 1

max_enemy_health = 10
max_enemy_speed = 7

max_enemy_spawned = 5
max_asteroid_spawned = 10

enemy_spawner_delay = 2





# ---------------------------------------------------------------------
# Main Game Functions



# Save game function
def save_game():
    global game_state, player_lives, score, ship_x, ship_y, ship_angle, bullets, enemies, enemy_bullets, asteroids, enemy_follows
    global asteroid_spawners, enemy_spawners, hearts, power_ups, active_power_up, power_up_end_time, invincible_end_time
    global last_enemy_spawn_time, last_spawn_time, difficulty_timer, enemy_spawner_delay, ship_frames, frame_index
    global current_enemy_health_max, current_enemy_speed, current_heart_spawn_chance, current_power_up_spawn_chance
    global double_damage, dmg_pwr_up_end_time, frozen_opacity, blinking_toggle, freeze_end_time, player_frozen
    global last_shot_time, shot_delay, asteroid_anim_index, asteroid_anim_counter, enemy_anim_index, enemy_anim_counter
    global last_enemy_follow_spawn_time

    save_data = {
        "game_state": game_state,
        "player_lives": player_lives,
        "score": score,
        "ship_x": ship_x,
        "ship_y": ship_y,
        "ship_angle": ship_angle,
        "bullets": bullets,
        "enemies": enemies,
        "enemy_bullets": enemy_bullets,
        "asteroids": asteroids,
        "enemy_follows": enemy_follows,
        "asteroid_spawners": asteroid_spawners,
        "enemy_spawners": enemy_spawners,
        "hearts": hearts,
        "power_ups": power_ups,
        "active_power_up": active_power_up,
        "power_up_end_time": power_up_end_time,
        "invincible_end_time": invincible_end_time,
        "last_enemy_spawn_time": last_enemy_spawn_time,
        "last_spawn_time": last_spawn_time,
        "last_enemy_follow_spawn_time": last_enemy_follow_spawn_time,
        "difficulty_timer": difficulty_timer,
        "enemy_spawner_delay": enemy_spawner_delay,
        "current_enemy_health_max": current_enemy_health_max,
        "current_enemy_speed": current_enemy_speed,
        "current_heart_spawn_chance": current_heart_spawn_chance,
        "current_power_up_spawn_chance": current_power_up_spawn_chance,
        "double_damage": double_damage,
        "dmg_pwr_up_end_time": dmg_pwr_up_end_time,
        "frozen_opacity": frozen_opacity,
        "blinking_toggle": blinking_toggle,
        "freeze_end_time": freeze_end_time,
        "player_frozen": player_frozen,
        "last_shot_time": last_shot_time,
        "shot_delay": shot_delay,
        "frame_index": frame_index,
        "asteroid_anim_index": asteroid_anim_index,
        "asteroid_anim_counter": asteroid_anim_counter,
        "enemy_anim_index": enemy_anim_index,
        "enemy_anim_counter": enemy_anim_counter
    }

    with open("savegame.pkl", "wb") as file:
        pickle.dump(save_data, file)
    print("Game saved.")





# Load game function
def load_game():
    global game_state, player_lives, score, ship_x, ship_y, ship_angle, bullets, enemies, enemy_bullets, asteroids, enemy_follows
    global asteroid_spawners, enemy_spawners, hearts, power_ups, active_power_up, power_up_end_time, invincible_end_time
    global last_enemy_spawn_time, last_spawn_time, difficulty_timer, enemy_spawner_delay, ship_frames, frame_index
    global current_enemy_health_max, current_enemy_speed, current_heart_spawn_chance, current_power_up_spawn_chance
    global double_damage, dmg_pwr_up_end_time, frozen_opacity, blinking_toggle, freeze_end_time, player_frozen
    global last_shot_time, shot_delay, asteroid_anim_index, asteroid_anim_counter, enemy_anim_index, enemy_anim_counter
    global last_enemy_follow_spawn_time

    try:
        with open("savegame.pkl", "rb") as file:
            save_data = pickle.load(file)

            game_state = save_data["game_state"]
            player_lives = save_data["player_lives"]
            score = save_data["score"]
            ship_x = save_data["ship_x"]
            ship_y = save_data["ship_y"]
            ship_angle = save_data["ship_angle"]
            bullets = save_data["bullets"]
            enemies = save_data["enemies"]
            enemy_bullets = save_data["enemy_bullets"]
            asteroids = save_data["asteroids"]
            enemy_follows = save_data["enemy_follows"]
            asteroid_spawners = save_data["asteroid_spawners"]
            enemy_spawners = save_data["enemy_spawners"]
            hearts = save_data["hearts"]
            power_ups = save_data["power_ups"]
            active_power_up = save_data["active_power_up"]
            power_up_end_time = save_data["power_up_end_time"]
            invincible_end_time = save_data["invincible_end_time"]
            last_enemy_spawn_time = save_data["last_enemy_spawn_time"]
            last_spawn_time = save_data["last_spawn_time"]
            last_enemy_follow_spawn_time = save_data["last_enemy_follow_spawn_time"]
            difficulty_timer = save_data["difficulty_timer"]
            enemy_spawner_delay = save_data["enemy_spawner_delay"]
            current_enemy_health_max = save_data["current_enemy_health_max"]
            current_enemy_speed = save_data["current_enemy_speed"]
            current_heart_spawn_chance = save_data["current_heart_spawn_chance"]
            current_power_up_spawn_chance = save_data["current_power_up_spawn_chance"]
            double_damage = save_data["double_damage"]
            dmg_pwr_up_end_time = save_data["dmg_pwr_up_end_time"]
            frozen_opacity = save_data["frozen_opacity"]
            blinking_toggle = save_data["blinking_toggle"]
            freeze_end_time = save_data["freeze_end_time"]
            player_frozen = save_data["player_frozen"]
            last_shot_time = save_data["last_shot_time"]
            shot_delay = save_data["shot_delay"]
            frame_index = save_data["frame_index"]
            asteroid_anim_index = save_data["asteroid_anim_index"]
            asteroid_anim_counter = save_data["asteroid_anim_counter"]
            enemy_anim_index = save_data["enemy_anim_index"]
            enemy_anim_counter = save_data["enemy_anim_counter"]

        load_highscore()
        print("Game loaded.")
    except FileNotFoundError:
        print("No saved game found.")





# Increase difficulty function
def increase_difficulty():
    global asteroid_spawners, enemy_spawners, current_enemy_health_max, current_enemy_speed, enemy_spawner_delay
    global current_heart_spawn_chance, current_power_up_spawn_chance

    if asteroid_spawners < max_asteroid_spawners:
        asteroid_spawners += 1
    elif current_enemy_health_max < max_enemy_health or current_enemy_speed < max_enemy_speed:
        current_enemy_health_max = min(current_enemy_health_max + enemy_health_increment, max_enemy_health)
        current_enemy_speed = min(current_enemy_speed + enemy_speed_increment, max_enemy_speed)
    
    if asteroid_spawners >= max_asteroid_spawners and enemy_spawners < max_enemy_spawners and enemy_spawner_delay <= 0:
        enemy_spawners += 1
        enemy_spawner_delay = 2
    else:
        enemy_spawner_delay -= 1

    if current_heart_spawn_chance < max_heart_spawn_chance:
        current_heart_spawn_chance = min(current_heart_spawn_chance + 0.05, max_heart_spawn_chance)
    if current_power_up_spawn_chance < max_power_up_spawn_chance:
        current_power_up_spawn_chance = min(current_power_up_spawn_chance + 0.05, max_power_up_spawn_chance)




# Start game function
def start_game():
    global game_state, player_lives, game_over, score, asteroid_spawners, enemy_spawners, enemy_follows, last_enemy_follow_spawn_time
    global current_enemy_health_max, current_enemy_speed, current_heart_spawn_chance, current_power_up_spawn_chance
    global difficulty_timer, player_frozen, frozen_opacity, invincible_end_time, last_shot_time, last_enemy_spawn_time
    global last_spawn_time, double_damage, dmg_pwr_up_end_time, active_power_up, power_up_end_time
    global ship_x, ship_y, ship_angle, bullets, enemies, enemy_bullets, asteroids, hearts, power_ups
    global enemy_spawner_delay

    game_state = STATE_PLAYING
    player_lives = 3
    game_over = False
    score = 0
    bullets.clear()
    enemies.clear()
    enemy_bullets.clear()
    asteroids.clear()
    hearts.clear()
    power_ups.clear()
    enemy_follows.clear()

    ship_x, ship_y = WIDTH // 2, HEIGHT // 2
    ship_angle = 90
    player_frozen = False
    frozen_opacity = 192
    invincible_end_time = 0
    last_shot_time = 0
    last_enemy_spawn_time = 0
    last_spawn_time = 0
    last_enemy_follow_spawn_time = 0

    difficulty_timer = time.time()

    active_power_up = None
    power_up_end_time = 0
    double_damage = False
    dmg_pwr_up_end_time = 0

    asteroid_spawners = 1
    enemy_spawners = 1

    current_enemy_health_max = ENEMY_HEALTH_MAX
    current_enemy_speed = ENEMY_SPEED

    current_heart_spawn_chance = HEART_SPAWN_CHANCE
    current_power_up_spawn_chance = POWER_UP_SPAWN_CHANCE

    enemy_spawner_delay = 2


# Quit game function
def quit_game():
    pygame.quit()
    sys.exit()


# ---------------------------------------------------------------------









# ---------------------------------------------------------------------
# Highscore / Score functions


# Load highscore function
def load_highscore():
    global highscore

    try:
        with open(highscore_file, "rb") as file:
            highscore = pickle.load(file)
    except FileNotFoundError:
        highscore = 0  


# Save highscore function
def save_highscore():
    with open(highscore_file, "wb") as file:
        pickle.dump(highscore, file)

    print("Highscore saved.")



# Update highscore function
def update_highscore():
    global score, highscore, game_over

    if game_over and score > highscore:
        highscore = score
        save_highscore()


# Add score function
def add_score(points):
    global score
    score += points



# ---------------------------------------------------------------------









# ---------------------------------------------------------------------
# Spawn functions



# Enemy spawner function
def create_enemies():
    for _ in range(enemy_spawners):
        if len(enemies) < max_enemy_spawned:
            create_enemy()





# Asteroid spawner function
def create_asteroids():
    for _ in range(asteroid_spawners):
        if len(asteroids) < max_asteroid_spawned:
            create_asteroid()




# Create enemy function
def create_enemy():
    side_x = random.choice(['left', 'right'])
    side_y = random.choice(['top', 'bottom'])

    if side_x == 'left':
        x = -ENEMY_WIDTH
    elif side_x == 'right':
        x = WIDTH

    if side_y == 'top':
        y = -ENEMY_HEIGHT
    elif side_y == 'bottom':
        y = HEIGHT

    health = random.randint(ENEMY_HEALTH_MIN, ENEMY_HEALTH_MAX)

    direction_angle = random.uniform(0, 360)  
    direction_radians = math.radians(direction_angle)

    dx = ENEMY_SPEED * math.cos(direction_radians)
    dy = ENEMY_SPEED * math.sin(direction_radians)

    next_shot_time = time.time() + random.randint(5, 10)
    next_direction_change = time.time() + 8

    enemies.append({
    "x": x, "y": y, "dx": dx, "dy": dy, "health": health,
    "frame": 0, "frame_counter": 0,
    "next_shot": next_shot_time,
    "next_direction_change": next_direction_change
    })



# Create enemy follower function
def create_enemy_follow():
    side_x = random.choice(['left', 'right'])
    side_y = random.choice(['top', 'bottom'])

    if side_x == 'left':
        x = -ENEMY_FOLLOW_WIDTH
    elif side_x == 'right':
        x = WIDTH
    if side_y == 'top':
        y = -ENEMY_FOLLOW_HEIGHT
    elif side_y == 'bottom':
        y = HEIGHT

    direction_angle = math.atan2(ship_y - y, ship_x - x)
    dx = ENEMY_FOLLOW_DEFAULT_SPEED * math.cos(direction_angle)
    dy = ENEMY_FOLLOW_DEFAULT_SPEED * math.sin(direction_angle)

    enemy_follows.append({
        "x": x, "y": y, "dx": dx, "dy": dy, "health": ENEMY_FOLLOW_HEALTH,
        "frame": 0, "frame_counter": 0, "speed": ENEMY_FOLLOW_DEFAULT_SPEED,
        "next_speed_increase": time.time() + 1
    })



# Create asteroid function
def create_asteroid():
    side_x = random.choice(['left', 'right'])
    side_y = random.choice(['top', 'bottom'])

    if side_x == 'left':
        x = -ASTEROID_FRAME_WIDTH
    elif side_x == 'right':
        x = WIDTH

    if side_y == 'top':
        y = -ASTEROID_FRAME_HEIGHT
    elif side_y == 'bottom':
        y = HEIGHT


    health = random.randint(MIN_ASTEROID_HEALTH, MAX_ASTEROID_HEALTH)

    scale = random.uniform(0.75, 2)

    angle = math.atan2(ship_y - y, ship_x - x)
    dx = (ASTEROID_SPEED / scale) * math.cos(angle)
    dy = (ASTEROID_SPEED / scale) * math.sin(angle)

    health = int(health * scale)

    asteroids.append({
        "x": x,
        "y": y,
        "dx": dx,
        "dy": dy,
        "health": health,
        "frame": 0,
        "scale": scale
    })



# Spawn enemy bullet function
def create_enemy_bullet(enemy_x, enemy_y, ship_x, ship_y):
    angle = math.atan2(ship_y - enemy_y, ship_x - enemy_x)

    enemy_bullet_dx = ENEMY_BULLET_SPEED * math.cos(angle)
    enemy_bullet_dy = ENEMY_BULLET_SPEED * math.sin(angle)

    enemy_bullets.append({
        "x": enemy_x,
        "y": enemy_y,
        "dx": enemy_bullet_dx,
        "dy": enemy_bullet_dy
    })



# Spawn heart function
def spawn_heart(x, y):
    hearts.append({
        "x": x, "y": y,
        "spawn_time": time.time(),  
        "blink": False 
    })


# Create asteroid explosion function
def create_asteroid_explosion(x, y, scale):
    explosions.append({
        "x": x,
        "y": y,
        "frame": 0,
        "frame_counter": 0,
        "alpha": 255,
        "scale": scale,
        "frames": asteroid_explosion_frames
    })


# Create enemy explosion function
def create_enemy_explosion(x, y):
    explosions.append({
        "x": x,
        "y": y,
        "frame": 0,
        "frame_counter": 0,
        "alpha": 255,
        "frames": explosion_frames  
    })

# Spawn power-up function
def spawn_random_power_up(x, y, scale):
    chance = POWER_UP_SPAWN_CHANCE * scale

    if random.random() < chance:
        power_up_type = random.choice(['shooting', 'shield', 'damage', 'heal'])
        power_ups.append({
            "x": x, "y": y, "type": power_up_type, "spawn_time": time.time(), "blink": False
        })


# ---------------------------------------------------------------------








# ---------------------------------------------------------------------
# Update functions


# Update power-ups function
def update_power_ups():
    """Aktualizuje power-upy a aplikuje jejich efekty na hráče."""
    global active_power_up, power_up_end_time, shot_delay, invincible_end_time, double_damage, dmg_pwr_up_end_time, player_lives

    for power_up in power_ups[:]:
        time_elapsed = time.time() - power_up["spawn_time"]
        if time_elapsed >= POWER_UP_DURATION:
            power_ups.remove(power_up)
        elif time_elapsed >= POWER_UP_DURATION - POWER_UP_BLINK_START:
            power_up["blink"] = not power_up["blink"]

        player_rect = pygame.Rect(ship_x - SHIP_WIDTH // 2, ship_y - SHIP_HEIGHT // 2, SHIP_WIDTH, SHIP_HEIGHT)
        power_up_rect = pygame.Rect(power_up["x"], power_up["y"], 48, 48)

        if active_power_up is None and player_rect.colliderect(power_up_rect) and power_up["type"] != "heal":
            power_ups.remove(power_up)
            active_power_up = power_up["type"]
            power_up_end_time = time.time() + POWER_UP_DURATION

            if active_power_up == "shooting":
                shot_delay = 0.35
            elif active_power_up == "shield":
                invincible_end_time = power_up_end_time
            elif active_power_up == "damage":
                double_damage = True
                dmg_pwr_up_end_time = power_up_end_time

        if player_rect.colliderect(power_up_rect) and power_up["type"] == "heal" and player_lives < MAX_LIVES:
            power_ups.remove(power_up)
            if player_lives < MAX_LIVES:
                player_lives = MAX_LIVES

    if active_power_up == "shooting" and time.time() >= power_up_end_time:
        active_power_up = None
        shot_delay = 1
    elif active_power_up == "shield" and time.time() >= power_up_end_time:
        active_power_up = None
        invincible_end_time = 0
    elif active_power_up == "damage" and time.time() >= dmg_pwr_up_end_time:
        active_power_up = None
        double_damage = False




# Update function for bullets and asteroids
def update_bullets_and_asteroids():

    global double_damage
    
    for bullet in bullets[:]:
        for asteroid in asteroids[:]:
            asteroid_rect = pygame.Rect(
                asteroid["x"],
                asteroid["y"],
                int(ASTEROID_FRAME_WIDTH * asteroid["scale"]),
                int(ASTEROID_FRAME_HEIGHT * asteroid["scale"])
            )

            bullet_rect = pygame.Rect(bullet["x"], bullet["y"], BULLET_WIDTH, BULLET_HEIGHT)

            if asteroid_rect.colliderect(bullet_rect):
                bullets.remove(bullet)

                damage = 2 if double_damage else 1

                asteroid["health"] -= damage

                if asteroid["health"] <= 0:
                    spawn_random_power_up(asteroid["x"], asteroid["y"], asteroid["scale"])
                    create_asteroid_explosion(asteroid["x"], asteroid["y"], asteroid["scale"])

                    asteroids.remove(asteroid)

                    add_score(ASTEROID_SCORE)
                break




# Update collision detection for player bullets and enemies
def update_bullets_and_enemies():

    global double_damage
    
    for bullet in bullets[:]:
        for enemy in enemies[:]:
            enemy_rect = pygame.Rect(enemy["x"], enemy["y"], ENEMY_WIDTH, ENEMY_HEIGHT)
            bullet_rect = pygame.Rect(bullet["x"], bullet["y"], BULLET_WIDTH, BULLET_HEIGHT)

            if enemy_rect.colliderect(bullet_rect):
                bullets.remove(bullet)

                damage = 2 if double_damage else 1

                enemy["health"] -= damage
        
                if enemy["health"] <= 0:
                    create_enemy_explosion(enemy["x"], enemy["y"])

                    enemies.remove(enemy)

                    if random.random() < HEART_SPAWN_CHANCE:
                        spawn_heart(enemy["x"], enemy["y"])

                    add_score(ENEMY_SCORE)
                break




# Update shooting enemies function
def update_shooting_enemies():
    global current_time

    for enemy in enemies:

        enemy["x"] += enemy["dx"]
        enemy["y"] += enemy["dy"]

        if enemy["x"] < -WRAP_BUFFER_X:
            enemy["x"] = WIDTH + WRAP_BUFFER_X
        elif enemy["x"] > WIDTH + WRAP_BUFFER_X:
            enemy["x"] = -WRAP_BUFFER_X
        if enemy["y"] < -WRAP_BUFFER_Y:
            enemy["y"] = HEIGHT + WRAP_BUFFER_Y
        elif enemy["y"] > HEIGHT + WRAP_BUFFER_Y:
            enemy["y"] = -WRAP_BUFFER_Y

        if current_time >= enemy["next_direction_change"]:
            new_direction_angle = random.uniform(0, 360)
            new_direction_radians = math.radians(new_direction_angle)

            enemy["dx"] = ENEMY_SPEED * math.cos(new_direction_radians)
            enemy["dy"] = ENEMY_SPEED * math.sin(new_direction_radians)

            enemy["next_direction_change"] = current_time + 8

        if current_time >= enemy["next_shot"]:

            enemy_center_x = enemy["x"] + ENEMY_WIDTH // 2
            enemy_center_y = enemy["y"] + ENEMY_HEIGHT // 2

            create_enemy_bullet(enemy_center_x, enemy_center_y, ship_x, ship_y)

            enemy["next_shot"] = current_time + random.uniform(ENEMY_BULLET_SPAWN_MIN, ENEMY_BULLET_SPAWN_MAX)

        enemy["frame_counter"] += 1

        if enemy["frame_counter"] >= ENEMY_ANIM_DELAY:
            enemy["frame"] = (enemy["frame"] + 1) % ENEMY_FRAME_COUNT
            enemy["frame_counter"] = 0



# Update Enemy Bullets Function
def update_enemy_bullets():

    global player_lives, game_over, invincible_end_time

    if active_power_up == "shield":
        return

    for enemy_bullet in enemy_bullets[:]:

        player_rect = pygame.Rect(ship_x - SHIP_WIDTH // 2, ship_y - SHIP_HEIGHT // 2, SHIP_WIDTH, SHIP_HEIGHT)
        bullet_rect = pygame.Rect(enemy_bullet["x"], enemy_bullet["y"], ENEMY_BULLET_WIDTH, ENEMY_BULLET_HEIGHT)

        if not game_over and current_time >= invincible_end_time and player_rect.colliderect(bullet_rect):
            enemy_bullets.remove(enemy_bullet)
            player_lives -= 1
            if player_lives <= 0:
                game_over = True
            else:
                invincible_end_time = current_time + invincibility_duration


# Update hearts function
def update_hearts():
    global player_lives

    for heart in hearts[:]:
        time_elapsed = current_time - heart["spawn_time"]

        if time_elapsed >= HEART_DURATION:
            hearts.remove(heart)  
        elif time_elapsed >= HEART_DURATION - HEART_BLINK_START:
            heart["blink"] = not heart["blink"]  

        
        player_rect = pygame.Rect(ship_x - SHIP_WIDTH // 2, ship_y - SHIP_HEIGHT // 2, SHIP_WIDTH, SHIP_HEIGHT)
        heart_rect = pygame.Rect(heart["x"], heart["y"], 16, 16)

        if player_rect.colliderect(heart_rect) and player_lives < MAX_LIVES:
            player_lives += 1
            hearts.remove(heart) 



# Update explosions
def update_explosions():
    for explosion in explosions[:]:
        explosion["frame_counter"] += 1

        if explosion["frame_counter"] >= EXPLOSION_ANIM_DELAY:
            explosion["frame"] += 1
            explosion["frame_counter"] = 0

        frames_left = len(explosion["frames"]) - explosion["frame"]
        explosion["alpha"] = max(0, int(255 * (frames_left / len(explosion["frames"]))))

        if explosion["frame"] >= len(explosion["frames"]) or explosion["alpha"] == 0:
            explosions.remove(explosion)




# Update enemy follower movement
def update_enemy_follows():
    global player_lives, game_over, invincible_end_time, player_frozen, freeze_end_time

    for enemy_follow in enemy_follows[:]:

        if time.time() >= enemy_follow["next_speed_increase"]:
            enemy_follow["speed"] = min(enemy_follow["speed"] + ENEMY_FOLLOW_SPEED_INCREMENT, ENEMY_FOLLOW_MAX_SPEED)
            enemy_follow["next_speed_increase"] = time.time() + 1

        direction_angle = math.atan2(ship_y - enemy_follow["y"], ship_x - enemy_follow["x"])

        enemy_follow["dx"] = enemy_follow["speed"] * math.cos(direction_angle)
        enemy_follow["dy"] = enemy_follow["speed"] * math.sin(direction_angle)

        enemy_follow["x"] += enemy_follow["dx"]
        enemy_follow["y"] += enemy_follow["dy"]

        enemy_follow["frame_counter"] += 1
        if enemy_follow["frame_counter"] >= ENEMY_ANIM_DELAY:
            enemy_follow["frame"] = (enemy_follow["frame"] + 1) % ENEMY_FOLLOW_FRAME_COUNT
            enemy_follow["frame_counter"] = 0

        player_rect = pygame.Rect(ship_x - SHIP_WIDTH // 2, ship_y - SHIP_HEIGHT // 2, SHIP_WIDTH, SHIP_HEIGHT)
        enemy_follow_rect = pygame.Rect(enemy_follow["x"], enemy_follow["y"], ENEMY_FOLLOW_WIDTH, ENEMY_FOLLOW_HEIGHT)

        if not game_over and current_time >= invincible_end_time and player_rect.colliderect(enemy_follow_rect):
            player_lives -= 1
            if player_lives <= 0:
                game_over = True
            else:
                invincible_end_time = current_time + invincibility_duration
                player_frozen = True
                freeze_end_time = current_time + 1

            create_enemy_explosion(enemy_follow["x"], enemy_follow["y"])
            enemy_follows.remove(enemy_follow)



# ---------------------------------------------------------------------










# ---------------------------------------------------------------------
# Player states update functions


# Update Player Invincibility Function
def check_players_invincibility():
    global blinking_toggle
    
    if current_time < invincible_end_time:
        blinking_toggle = not blinking_toggle
    else:
        blinking_toggle = False


# Update Frozen Player Function
def check_if_player_frozen():
    global frozen_opacity, player_frozen, freeze_end_time, blinking_toggle
    
    if player_frozen:
        if current_time >= freeze_end_time:
            player_frozen = False
            frozen_opacity = 192
        else:
            blinking_toggle = not blinking_toggle


# Update Invincibility Function
def update_invincibility():
    global blinking_toggle

    if current_time < invincible_end_time:
        blinking_toggle = not blinking_toggle
    else:
        blinking_toggle = False


# Update Player State Function
def update_player_state():
    global player_frozen, freeze_end_time, frozen_opacity, blinking_toggle

    if player_frozen and current_time >= freeze_end_time:
        player_frozen = False
        frozen_opacity = 192  
    elif player_frozen:
        blinking_toggle = not blinking_toggle

# ---------------------------------------------------------------------










# ---------------------------------------------------------------------
# Collision detection functions

# Check for collision with enemy bullets
def enemy_bullets_player():
    global player_lives, game_over, invincible_end_time

    for enemy_bullet in enemy_bullets[:]:
            enemy_bullet["x"] += enemy_bullet["dx"]
            enemy_bullet["y"] += enemy_bullet["dy"]

            player_rect = pygame.Rect(ship_x - SHIP_WIDTH // 2, ship_y - SHIP_HEIGHT // 2, SHIP_WIDTH, SHIP_HEIGHT)
            bullet_rect = pygame.Rect(enemy_bullet["x"], enemy_bullet["y"], ENEMY_BULLET_WIDTH, ENEMY_BULLET_HEIGHT)

            if not game_over and current_time >= invincible_end_time and player_rect.colliderect(bullet_rect):
                enemy_bullets.remove(enemy_bullet)
                player_lives -= 1
                if player_lives <= 0:
                    game_over = True
                else:
                    invincible_end_time = current_time + invincibility_duration



# Check for collision with enemies
def check_enemy_collision():
    global player_lives, game_over, invincible_end_time, player_frozen, freeze_end_time

    if active_power_up == "shield" or player_frozen or current_time < invincible_end_time:
        return

    player_rect = pygame.Rect(ship_x - SHIP_WIDTH // 2, ship_y - SHIP_HEIGHT // 2, SHIP_WIDTH, SHIP_HEIGHT)

    for enemy in enemies[:]:
        enemy_rect = pygame.Rect(enemy["x"], enemy["y"], ENEMY_WIDTH, ENEMY_HEIGHT)

        if player_rect.colliderect(enemy_rect):
            player_lives -= 1

            if player_lives <= 0:
                game_over = True
            else:
                invincible_end_time = current_time + invincibility_duration

            angle = math.atan2(enemy["y"] - ship_y, enemy["x"] - ship_x)

            enemy["dx"] = ENEMY_SPEED * math.cos(angle)
            enemy["dy"] = ENEMY_SPEED * math.sin(angle)

            player_frozen = True
            freeze_end_time = current_time + 2



# Check for collision with asteroids
def check_asteroid_collision():
    global player_frozen, freeze_end_time, player_lives, invincible_end_time

    for asteroid in asteroids:
        asteroid["x"] += asteroid["dx"]
        asteroid["y"] += asteroid["dy"]

        if asteroid["x"] < -ASTEROID_FRAME_WIDTH:
            asteroid["x"] = WIDTH
        elif asteroid["x"] > WIDTH:
            asteroid["x"] = -ASTEROID_FRAME_WIDTH
        if asteroid["y"] < -ASTEROID_FRAME_HEIGHT:
            asteroid["y"] = HEIGHT
        elif asteroid["y"] > HEIGHT:
            asteroid["y"] = -ASTEROID_FRAME_HEIGHT

    if active_power_up == "shield" or player_frozen or invincible_end_time > current_time:
        return

    player_rect = pygame.Rect(ship_x - SHIP_WIDTH // 2, ship_y - SHIP_HEIGHT // 2, SHIP_WIDTH, SHIP_HEIGHT)

    for asteroid in asteroids:
        asteroid_rect = pygame.Rect(
            asteroid["x"],
            asteroid["y"],
            int(ASTEROID_FRAME_WIDTH * asteroid["scale"]),
            int(ASTEROID_FRAME_HEIGHT * asteroid["scale"])
        )

        if player_rect.colliderect(asteroid_rect):
            direction_angle = math.atan2(asteroid["y"] - ship_y, asteroid["x"] - ship_x)

            asteroid["dx"] = ASTEROID_SPEED * math.cos(direction_angle) / asteroid["scale"]
            asteroid["dy"] = ASTEROID_SPEED * math.sin(direction_angle) / asteroid["scale"]

            freeze_duration = asteroid["scale"] * 2
            player_frozen = True
            freeze_end_time = current_time + freeze_duration


# Check for collision with enemy followers
def check_enemy_follow_collision():
    for bullet in bullets[:]:
        for enemy_follow in enemy_follows[:]:

            enemy_follow_rect = pygame.Rect(
                enemy_follow["x"] - ENEMY_FOLLOW_WIDTH // 2,
                enemy_follow["y"] - ENEMY_FOLLOW_HEIGHT // 2,
                ENEMY_FOLLOW_WIDTH,
                ENEMY_FOLLOW_HEIGHT
            )

            bullet_rect = pygame.Rect(bullet["x"], bullet["y"], BULLET_WIDTH, BULLET_HEIGHT)

            if enemy_follow_rect.colliderect(bullet_rect):
                bullets.remove(bullet)

                enemy_follow["health"] -= 1

                if enemy_follow["health"] <= 0:
                    create_enemy_explosion(enemy_follow["x"], enemy_follow["y"])
                    enemy_follows.remove(enemy_follow)

                    if random.random() < ENEMY_FOLLOW_HEART_SPAWN_CHANCE:
                        spawn_heart(enemy_follow["x"], enemy_follow["y"])

                    add_score(ENEMY_FOLLOW_SCORE)

                break


# ---------------------------------------------------------------------







# ---------------------------------------------------------------------
# Animation update functions

def update_asteroid_animation():
    global asteroid_anim_index, asteroid_anim_counter
    
    asteroid_anim_counter += 1
        
    if asteroid_anim_counter >= ASTEROID_ANIM_DELAY:
        asteroid_anim_index = (asteroid_anim_index + 1) % len(asteroid_frames)
        asteroid_anim_counter = 0


def update_player_animation():
    global frame_index, frame_counter
    
    frame_counter += 1

    if frame_counter >= frame_delay:
        frame_index = (frame_index + 1) % FRAME_COUNT
        frame_counter = 0


def update_enemy_shooter_animation():
    global enemy_anim_index, enemy_anim_counter
    
    enemy_anim_counter += 1
    
    if enemy_anim_counter >= ENEMY_ANIM_DELAY:
        enemy_anim_index = (enemy_anim_index + 1) % ENEMY_FRAME_COUNT
        enemy_anim_counter = 0

# ---------------------------------------------------------------------












# ---------------------------------------------------------------------
# Draw functions


# Draw btns function
def draw_button(text, x, y, width, height, callback):
    mouse_pos = pygame.mouse.get_pos()
    mouse_pressed = pygame.mouse.get_pressed()

    button_rect = pygame.Rect(x, y, width, height)
    
    if button_rect.collidepoint(mouse_pos):
        pygame.draw.rect(screen, (170, 170, 170), button_rect)
        if mouse_pressed[0]:
            callback() # Callin' the callback function
    else:
        pygame.draw.rect(screen, (200, 200, 200), button_rect)
    
    text_surf = score_font.render(text, True, (0, 0, 0))
    text_rect = text_surf.get_rect(center=button_rect.center)

    screen.blit(text_surf, text_rect)



# Draw hearts function
def draw_hearts():  
    for heart in hearts:
        if heart["blink"]:
            heart_image.set_alpha(HEART_OPACITY)
        else:
            heart_image.set_alpha(255)
        screen.blit(heart_image, (heart["x"], heart["y"]))


# Draw power-ups function
def draw_power_ups():
    for power_up in power_ups:
        alpha = 128 if power_up["blink"] else 255

        if power_up["type"] == "shooting":
            shooting_pwr_up_img.set_alpha(alpha)
            screen.blit(shooting_pwr_up_img, (power_up["x"], power_up["y"]))
        elif power_up["type"] == "shield":
            shield_pwr_up_img.set_alpha(alpha)
            screen.blit(shield_pwr_up_img, (power_up["x"], power_up["y"]))
        elif power_up["type"] == "damage":
            dmg_pwr_up_img.set_alpha(alpha)
            screen.blit(dmg_pwr_up_img, (power_up["x"], power_up["y"]))
        elif power_up["type"] == "heal":
            heal_pwr_up_img.set_alpha(alpha)
            screen.blit(heal_pwr_up_img, (power_up["x"], power_up["y"]))




# Draw asteroid explosions function
def draw_asteroid_explosions():
    global reduced_height
    
    for explosion in explosions:
        if "frames" not in explosion or explosion["frames"] != asteroid_explosion_frames:
            continue 
        
        original_width = ASTEROID_EXPLOSION_FRAME_WIDTH
        original_height = ASTEROID_EXPLOSION_FRAME_HEIGHT

        reduced_width = max(0, original_width - 256)
        reduced_height = max(0, original_height - 256)

        scaled_width = int(reduced_width * explosion["scale"])
        scaled_height = int(reduced_width * explosion["scale"])

        explosion_frame = explosion["frames"][explosion["frame"]].copy()
        explosion_frame = pygame.transform.scale(explosion_frame, (scaled_width, scaled_height))

        explosion_frame.set_alpha(explosion["alpha"])

        explosion_x = explosion["x"] - scaled_width // 2 + 40
        explosion_y = explosion["y"] - scaled_height // 2 + 40

        screen.blit(explosion_frame, (explosion_x, explosion_y))


# Draw enemy followers with rotation
def draw_enemy_follows():
    for enemy_follow in enemy_follows:
       
        angle = math.degrees(math.atan2(ship_y - enemy_follow["y"], ship_x - enemy_follow["x"]))
        
        rotated_follow = pygame.transform.rotate(enemy_follow_frames[enemy_follow["frame"]], angle)
        
        follow_rect = rotated_follow.get_rect(center=(enemy_follow["x"], enemy_follow["y"]))
        screen.blit(rotated_follow, follow_rect.topleft)

# Draw enemy explosions function
def draw_enemy_explosions():
    for explosion in explosions:
        if "frames" not in explosion or explosion["frames"] != explosion_frames:
            continue  

        explosion_frame = explosion_frames[explosion["frame"]].copy()
        explosion_frame.set_alpha(explosion["alpha"])

        explosion_x = explosion["x"] + ENEMY_WIDTH // 2 - EXPLOSION_SCALED_WIDTH // 2
        explosion_y = explosion["y"] + ENEMY_HEIGHT // 2 - EXPLOSION_SCALED_HEIGHT // 2

        screen.blit(explosion_frame, (explosion_x, explosion_y))




# Update the game loop to call the separated functions
def draw_explosions():
    draw_asteroid_explosions()
    draw_enemy_explosions()



# ---------------------------------------------------------------------








# ---------------------------------------------------------------------
# Change colors of sprites functions


# Blue tint for sprite function
def apply_blue_tint(surface):
    tinted_surface = surface.copy()
    tinted_surface.fill((0, 0, 255), special_flags=pygame.BLEND_RGB_ADD)
    
    return tinted_surface

# Green tint for sprite function
def apply_green_tint(surface):
    tinted_surface = surface.copy()
    tinted_surface.fill((0, 255, 0), special_flags=pygame.BLEND_RGB_ADD)

    return tinted_surface

# Red tint for sprite function
def apply_red_tint(surface):
    tinted_surface = surface.copy()
    tinted_surface.fill((255, 0, 0), special_flags=pygame.BLEND_RGB_ADD)

    return tinted_surface

# ---------------------------------------------------------------------





# Load highscore
load_highscore()

# Game loop
running = True
while running:
    current_time = time.time()

    # Event handling
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                if game_state == STATE_PLAYING:
                    game_state = STATE_MENU
                else:
                    pygame.quit()
                    sys.exit()
            if game_state == STATE_PLAYING and event.key == pygame.K_s and not game_over:
                save_game()
            if game_state == STATE_PLAYING:
                if game_over and event.key == pygame.K_r:
                    start_game()  
                if not game_over and not player_frozen and event.key == pygame.K_SPACE:
                    if current_time - last_shot_time >= shot_delay:
                        radians = math.radians(ship_angle)

                        bullet_dx = BULLET_SPEED * math.cos(radians)
                        bullet_dy = -BULLET_SPEED * math.sin(radians)

                        bullet_x = ship_x
                        bullet_y = ship_y

                        bullets.append({"x": bullet_x, "y": bullet_y, "dx": bullet_dx, "dy": bullet_dy, "angle": ship_angle})

                        last_shot_time = current_time
                        #spawn_random_power_up(bullet_x, bullet_y, 100)

    screen.fill((0, 0, 0))

    # Menu state
    if game_state == STATE_MENU:
        draw_button("Start", WIDTH // 2 - 100, HEIGHT // 2 - 100, 200, 60, start_game)
        draw_button("Load", WIDTH // 2 - 100, HEIGHT // 2 + 0, 200, 60, load_game)
        draw_button("Quit", WIDTH // 2 - 100, HEIGHT // 2 + 100, 200, 60, quit_game)
    # Playing state
    elif game_state == STATE_PLAYING: 
        
        # Increase difficulty
        if current_time - difficulty_timer >= difficulty_interval:
            increase_difficulty()
            difficulty_timer = current_time
        
        # Event handling
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    game_state = STATE_MENU
                if game_over and event.key == pygame.K_r:
                    start_game()
                

        # Player movement
        if not game_over and not player_frozen:
            keys = pygame.key.get_pressed()
            if keys[pygame.K_LEFT]:
                ship_angle += ROTATION_SPEED
            if keys[pygame.K_RIGHT]:
                ship_angle -= ROTATION_SPEED
            if keys[pygame.K_UP]:
                radians = math.radians(ship_angle)
                ship_x += SHIP_SPEED * math.cos(radians)
                ship_y -= SHIP_SPEED * math.sin(radians)

        # Player state functions
        check_players_invincibility()

        check_if_player_frozen()

        # Update functions
        update_invincibility()

        update_player_state()

        update_enemy_bullets()

        for enemy_bullet in enemy_bullets:
            enemy_bullet["x"] += enemy_bullet["dx"]
            enemy_bullet["y"] += enemy_bullet["dy"]

        # Remove bullets that are off-screen
        enemy_bullets = [
            eb for eb in enemy_bullets if 0 <= eb["x"] <= WIDTH and 0 <= eb["y"] <= HEIGHT
        ]

        update_player_animation()

        update_asteroid_animation()

        update_enemy_shooter_animation()

        update_hearts()

        update_power_ups()

        update_explosions()

        update_enemy_follows()

        for bullet in bullets:
            bullet["x"] += bullet["dx"]
            bullet["y"] += bullet["dy"]

        # Remove bullets that are off-screen
        bullets = [b for b in bullets if 0 <= b["x"] <= WIDTH and 0 <= b["y"] <= HEIGHT]

        # Wrap around the screen edges
        if ship_x < -WRAP_BUFFER_X:
            ship_x = WIDTH + WRAP_BUFFER_X
        elif ship_x > WIDTH + WRAP_BUFFER_X:
            ship_x = -WRAP_BUFFER_X
        if ship_y < -WRAP_BUFFER_Y:
            ship_y = HEIGHT + WRAP_BUFFER_Y
        elif ship_y > HEIGHT + WRAP_BUFFER_Y:
            ship_y = -WRAP_BUFFER_Y

        # Create new enemies and asteroids
        if current_time - last_enemy_spawn_time >= ENEMY_SPAWN_DELAY:
            create_enemies()
            last_enemy_spawn_time = current_time

        if current_time - last_spawn_time >= ASTEROID_SPAWN_DELAY:
            create_asteroids()
            last_spawn_time = current_time

        if current_time - last_enemy_follow_spawn_time >= ENEMY_FOLLOW_SPAWN_DELAY:
            create_enemy_follow()
            last_enemy_follow_spawn_time = current_time

        # Collisions functions
        enemy_bullets_player()

        check_asteroid_collision()

        update_shooting_enemies()

        update_bullets_and_asteroids()

        update_bullets_and_enemies()

        check_enemy_collision()

        check_enemy_follow_collision()
    
        rotated_ship = pygame.transform.rotate(ship_frames[frame_index], ship_angle)
        rotated_rect = rotated_ship.get_rect(center=(ship_x, ship_y))

        # Update player power-up state
        if active_power_up == "shooting":
            ship_surface = apply_green_tint(rotated_ship)
        elif active_power_up == "shield":
            ship_surface = apply_blue_tint(rotated_ship)
        elif active_power_up == "damage":
            ship_surface = apply_red_tint(rotated_ship)
        else:
            ship_surface = rotated_ship.copy()

        # Apply invincibility effect
        if current_time < invincible_end_time:
            ship_surface.set_alpha(frozen_opacity if blinking_toggle else 128)

        # Apply frozen effect
        if player_frozen:
            ship_surface.set_alpha(frozen_opacity if blinking_toggle else 128)

        # Draw functions
        screen.blit(background, (0, 0))

        # Draw score
        font = pygame.font.Font(None, 48)  
        score_text = font.render(f"Score: {score}", True, (255, 255, 255))  
        screen.blit(score_text, (WIDTH // 2 - 90, 120))  

        font_small = pygame.font.Font(None, 36)  
        highscore_text = font_small.render(f"HS: {highscore}", True, (255, 255, 255))
        screen.blit(highscore_text, (WIDTH // 2 - 90, 170)) 

        draw_hearts()

        draw_power_ups()

        draw_enemy_follows()

        screen.blit(ship_surface, rotated_ship.get_rect(center=(ship_x, ship_y)).topleft)


        for bullet in bullets:
            if double_damage:
                screen.blit(apply_red_tint(bullet_image), (bullet["x"], bullet["y"]))
            else:
                screen.blit(bullet_image, (bullet["x"], bullet["y"]))

    
        for enemy in enemies:
            screen.blit(enemy_frames[enemy["frame"]], (enemy["x"], enemy["y"]))


       
        for asteroid in asteroids:
            scaled_image = pygame.transform.scale(
                asteroid_frames[asteroid_anim_index],
                (int(ASTEROID_FRAME_WIDTH * asteroid["scale"]),
                int(ASTEROID_FRAME_HEIGHT * asteroid["scale"]))
            )

            screen.blit(scaled_image, (asteroid["x"], asteroid["y"]))

      
        for enemy_bullet in enemy_bullets:
            screen.blit(enemy_bullet_image, (enemy_bullet["x"], enemy_bullet["y"]))

 

        for i in range(player_lives):
            screen.blit(heart_image, (ship_x - 33 + i * 20, ship_y - SHIP_HEIGHT // 2 - 32))


        draw_explosions()

        # Draw game over screen
        if game_over:
       
            font = pygame.font.Font(None, 74)
            text = font.render("GAME OVER", True, (255, 0, 0))
            text_rect = text.get_rect(center=(WIDTH // 2, HEIGHT // 2))
            screen.blit(text, text_rect)

            sub_font = pygame.font.Font(None, 48)
            sub_text = sub_font.render("Press R", True, (255, 255, 255))
            sub_rect = sub_text.get_rect(center=(WIDTH // 2, HEIGHT // 2 + text_rect.height + 20))
            screen.blit(sub_text, sub_rect)

            update_highscore()

    # Update display
    pygame.display.flip()
    clock.tick(FPS)