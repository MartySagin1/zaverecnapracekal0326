<!-- .gitlab/issue_templates/issue_template.md -->

### Summary
<!-- Provide a concise summary of the issue -->

### Steps to reproduce
<!-- Describe the exact steps to reproduce the issue -->

1. Step 1
2. Step 2
3. Step 3

### Expected behavior
<!-- Describe what you expected to happen -->

### Actual behavior
<!-- Describe what actually happened -->

### Additional information
<!-- Add any other context, logs, or screenshots about the issue here -->
