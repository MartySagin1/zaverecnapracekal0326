# Závěrečná Práce

## Popis projektu

Závěrečná Práce je projekt zaměřený na automatizaci procesů vývoje softwaru pomocí nástrojů kontinuální integrace a nasazení (CI/CD) ve vývojovém prostředí GitLab.

### Obsah

1. [Popis projektu](#popis-projektu)
2. [Funkce](#funkce)
3. [Instalace](#instalace)
4. [Použití](#použití)
5. [Contributing](#contributing)
6. [Licence](#licence)

## Funkce

Projekt Závěrečná Práce poskytuje:

- **Automatizované Testování Kvality**: Používáme nástroje jako markdownlint pro kontrolu kvality Markdown souborů a pytest pro spuštění testů a kontrolu pokrytí kódu.
- **Linting Pro Každý Jazyk**: Používáme nástroje jako Flake8 pro kontrolu syntaxe Python kódu a pylint pro linting Python souborů.
- **Kontrola Aktualizací**: Využíváme nástroj pip-audit k ověření dostupných aktualizací a bezpečnostních aktualizací pro Python moduly.
- **Publikace HTML Reportů**: Generujeme HTML reporty z testů a pokrytí kódu, které jsou zveřejněny na GitLab Pages.

## Instalace

Pro instalaci projektu postupujte následovně:

1. Naklonujte tento repozitář na své lokální zařízení.
2. Nainstalujte nezbytné závislosti pomocí příkazu `pip install -r requirements.txt`.

## Použití

Po instalaci můžete spustit následující příkazy:

1. Pro spuštění testů a kontrolu kvality kódu:

   - `gitlab-runner exec docker test_markdown`
   - `gitlab-runner exec docker run_tests`
   - `gitlab-runner exec docker syntax_python`
   - `gitlab-runner exec docker lint_python`

2. Pro kontrolu aktualizací Python modulů:

   - `gitlab-runner exec docker check_updates`

3. Pro publikaci HTML reportů na GitLab Pages:

   - `gitlab-runner exec docker pages`

## Contributing

Přispění k projektu je vítáno! Pro příspěvky postupujte následovně:

1. Vytvořte novou větev s názvem odpovídajícím vaší funkci (`git checkout -b feature/NovaFunkce`).
2. Proveďte své změny.
3. Commitněte své změny (`git commit -am 'Přidána nová funkce: NovaFunkce'`).
4. Pushněte svou větev do repozitáře (`git push origin feature/NovaFunkce`).
5. Vytvořte nový pull request.

## Licence

[MIT Licence](LICENSE)
